
```
方法中 a作为除数，b作为被除数
def division(a,b)
  if b > 0
    return (a / b.to_f)
  else
    return 0
  end
end
```
可以类似修改如下：
```
def division(a,b)
  a / [b.to_f, 1.0].max
end

```